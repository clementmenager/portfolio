const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/projet.js', 'public/js')
    .js('resources/js/veille.js', 'public/js')
    .sass('resources/scss/app.scss', 'public/css')
    .sass('resources/scss/projet.scss', 'public/css')
    .sass('resources/scss/veille.scss', 'public/css')
    .sass('resources/scss/transition.scss', 'public/css')

