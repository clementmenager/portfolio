window.$ = window.jQuery = require('jquery');


require('fullpage.js')


// FULL PAGE -------------------------
new fullpage('#fullPage', {
    autoScrolling:true,
    easingcss3: 'ease-out',
    licenseKey: 'OPEN-SOURCE-GPLV3-LICENSE',
});

//Cursor
const links = document.querySelectorAll('a');
links.forEach(link => {
    link.addEventListener('mouseenter', function(e){
        cursor.classList.remove("unactive")
        cursor.classList.add("active")
    })
    link.addEventListener('mouseleave', function(e){
        cursor.classList.remove("active")
        cursor.classList.add("unactive")
    })
})
const cursor = document.querySelector('.cursor');

document.addEventListener("mousemove" , function (e){
    cursor.style.left = e.pageX + 'px';
    cursor.style.top = e.pageY + 'px';

    //PARALLAX ---------------------------
    this.querySelectorAll('.layer').forEach(layer=>{
        const speed=layer.getAttribute('data-speed')

        const x = (window.innerWidth - e.pageX*speed)/100

        layer.style.transform = `translateX(${x}px)`
    })
    this.querySelectorAll('.layer2').forEach(layer=>{
        const speed=layer.getAttribute('data-speed')

        const x = (window.innerWidth - e.pageX*speed)/100
        const y = (window.innerHeight - e.pageY*speed)/100

        layer.style.transform = `translateX(${x}px)translateY(${y}px)`
    })
});


//navbar-effect -------------------------
//ACTIVE NAV ON CLICK
$('nav a').on('click',function(){
    $("nav a").removeClass('active');
    // $(this).addClass('active');
});
//ACTIVE NAV ON SCROLL --------------------------------------------------------
$('nav a[href="' + window.location.hash + '"]').addClass('active');
$(window).on('hashchange', function () {
    $("nav a").removeClass('active')
    $('nav a[href="' + window.location.hash + '"]').addClass('active');
});


//Load ----------------------------
window.onload=()=>{
    const transition_el = document.querySelector('.transition');
    console.log(transition_el);
    setTimeout(() => {
        $("div").removeClass('is-active');
    }, 1250);
}

//Couleur background ----------------
$(window).on('hashchange', function () {
    if(window.location.hash == "#accueil"){
        $(".section").css("background", "#FFD670");
    }
    if(window.location.hash == "#qui-suis-je"){
        $(".section").css("background", "#92BADD");
    }
    if(window.location.hash == "#mes-projets"){
        $(".section").css("background", "#EEE1D2");
    }
    if(window.location.hash == "#me-contacter"){
        $(".section").css("background", "#BCDACE");
    }
});

if(window.location.hash == ""){
    $(".section").css("background", "#FFD670");
}
if(window.location.hash == "#accueil"){
    $(".section").css("background", "#FFD670");
}
if(window.location.hash == "#qui-suis-je"){
    $(".section").css("background", "#92BADD");
}
if(window.location.hash == "#mes-projets"){
    $(".section").css("background", "#EEE1D2");
}
if(window.location.hash == "#me-contacter"){
    $(".section").css("background", "#BCDACE");
}
