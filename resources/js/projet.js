window.$ = window.jQuery = require('jquery');


require('fullpage.js')


// FULL PAGE -------------------------
new fullpage('#fullPage', {
    autoScrolling:true,
    navigation:true,
    easingcss3: 'ease-out',
    licenseKey: 'OPEN-SOURCE-GPLV3-LICENSE'
});

//Cursor
const links = document.querySelectorAll('a');
    links.forEach(link => {
        link.addEventListener('mouseenter', function(e){
            cursor.classList.remove("unactive")
            cursor.classList.add("active")
        })
        link.addEventListener('mouseleave', function(e){
            cursor.classList.remove("active")
            cursor.classList.add("unactive")
        })
    })
const cursor = document.querySelector('.cursor');

document.addEventListener("mousemove" , function (e){
    cursor.style.left = e.pageX + 'px';
    cursor.style.top = e.pageY + 'px';
})

//Load ----------------------------
window.onload=()=>{
    const transition_el = document.querySelector('.transition');
    console.log(transition_el);
    setTimeout(() => {
        $("div").removeClass('is-active');
    }, 1250);
}

//Couleur background -------------------
$(window).on('hashchange', function () {
    if(window.location.hash == "#1"){
        $(".section").css("background", "#CEC0A1");
    }
    if(window.location.hash == "#2"){
        $(".section").css("background", "#FFE299");
    }
});
console.log(window.location.hash);
if(window.location.hash == ""){
    $(".section").css("background", "#CEC0A1");
}
if(window.location.hash == "#1"){
    $(".section").css("background", "#CEC0A1");
}
if(window.location.hash == "#2"){
    $(".section").css("background", "#FFE299");
}
