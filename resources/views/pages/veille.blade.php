@extends('layout.veille')

@section('title',"")

@section('content')






<div id="fullPage">
    <div class="section" data-anchor="1">
        <div>
            <div>
                <img src="{{ asset('images/vuejs.png') }}" alt="" srcset="">
            </div>
            <div>
                <h1>Vue.js</h1>
                <p>
                    À venir

                    <br><br><br><a href="" target="_blank" class="btn vue">En savoir plus</a>
                </p>
            </div>
        </div>
    </div>

    <div class="section" data-anchor="2">
        <div>
            <div>
                <img src="{{ asset('images/laravel.png') }}" alt="" srcset="">
            </div>
            <div>
                <h1>Laravel</h1>
                <p>
                    À venir

                    <br><br><br><a href="" target="_blank" class="btn Laravel">En savoir plus</a>
                </p>
            </div>
        </div>


    </div>
</div>
<div class="cursor"></div>
@endsection
