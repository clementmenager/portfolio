@extends('layout.main')

@section('title',"")

@section('content')
<div id="fullPage">
    <div class="section s1" data-anchor="accueil">
        <img src="{{ asset('images/nuage1.svg') }}" alt="" data-speed="8" class="layer">
        <img src="{{ asset('images/soleil.svg') }}" alt="" data-speed="-3" class="layer2">
        <img src="{{ asset('images/nuage2.svg') }}" alt="" data-speed="-10" class="layer">
        {{-- <img src="{{ asset('images/arbre2.svg') }}" alt=""> --}}
        <img src="{{ asset('images/nuage4.svg') }}" alt="" data-speed="7" class="layer">
        {{-- <img src="{{ asset('images/arbre1.svg') }}" alt=""> --}}
        <h1 class="layer2" data-speed="2">Clément Ménager</h1>
        {{-- <img src="{{ asset('images/montagne.svg') }}" alt=""> --}}
        <img src="{{ asset('images/nuage3.svg') }}" alt="" data-speed="5" class="layer">
    </div>


    <div class="section s2" data-anchor="qui-suis-je">
        <div>
            <div>
                <div class="blob">
                    <img src="{{ asset('images/Me3.png') }}" alt="" srcset="">
                </div>
            </div>
            <div>
                <h1>Qui suis-je ?</h1>
                <p>Actuellement en deuxième année BTS SIO (Services Informatiques aux Organisations) option SLAM (Solutions Logicielles et Applications Métier). Je souhaite poursuivre en licence orientée développement UI UX afin de pouvoir créer puis réaliser toutes sortes de sites web innovants.

                </p>
                <a href="{{ asset('fichier/CV.pdf') }}" class="btn" target="_blank">Voir mon CV</a>
            </div>
        </div>
    </div>


    <div class="section s3" data-anchor="mes-projets">
        <h1>Mes projets</h1>
        <p>Voici quelques exemples de mes réalisations / projets que j'ai pu réaliser.</p>
        <div class="content">
            <div class="card">
                <img src="{{ asset('images/projet-artdelapierre.png') }}" alt="">
                <div class="txt">
                    <h1>Art de la pierre</h1>
                    <p>Projet Stage</p>
                    <a href="{{ route('projet')."#1" }}" class="link">Voir le projet</a>
                </div>
            </div>
            <div class="card">
                <img src="{{ asset('images/projet-portfolio.png') }}" alt="">
                <div class="txt">
                    <h1>Mon portfolio</h1>
                    <p>Projet Personnel</p>
                    <a href="{{ route('projet')."#2" }}" class="link">Voir le projet</a>
                </div>
            </div>
        </div>

        <a href="{{ route('projet') }}" class="btn">Voir tous les projets</a>
    </div>

    <div class="section s4" data-anchor="me-contacter">
        <div class="container">

            <h1>Me contacter</h1>
            <p>Vous pouvez me contacter ci-dessous pour toutes propositions (stage, alternance, proposition d'embauche).
                Je vous répondrai avec le plus cours des délais.
            </p>
            <a href="mailto:contact@clement-menager.tk" class="link">contact@clement-menager.tk</a>
            <p></p>
            <a href="https://gitlab.com/clementmenager" target="_blank" class="gitlab"> <img src="{{ asset('images/gitlab.svg') }}" alt=""></a>
            <a href="https://www.linkedin.com/in/cl%C3%A9ment-m%C3%A9nager-0287a0209/" target="_blank" class="linkedin"><img src="{{ asset('images/linkedin2.svg') }}" alt=""></a>

        </div>

    </div>
</div>
<div class="cursor"></div>
@endsection
