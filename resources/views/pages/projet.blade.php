@extends('layout.second')

@section('title',"")

@section('content')
<div id="fullPage">
    <div class="section section-projet" data-anchor="1">
        <div>
            <div>
                <img src="{{ asset('images/projet-artdelapierre.png') }}" alt="" srcset="">
            </div>
            <div>
                <h1>Artdelapierre</h1>
                <p>Artdelapierre est un projet que j'ai effectué de A à Z lors de mon stage de première année de BTS SIO effectué chez <a href="https://latoile.dev/" class="link art">Latoile.dev</a>, ou j'ai utilisé le framework <a href="https://laravel.com/" class="link art">Laravel</a>. Le client était un tailleur de pierre et maçon. Il souhaitait un site vitrine en français et anglais pour faire la promotion de ces services en Creuse.
                    <br><br><br><a href="https://artdelapierre.net/" target="_blank" class="btn art">Voir le site</a>
                </p>
            </div>
        </div>
    </div>

    <div class="section section-projet" data-anchor="2">
        <div>
            <div>
                <img src="{{ asset('images/projet-portfolio.png') }}" alt="" srcset="">
            </div>
            <div>
                <h1>Portfolio</h1>
                <p>
                    Voici mon portfolio en constante évolution, car celon moi, mon portfolio reflaite les compétences que j'ai pu aquérir lors de mes expériences et autres forme d'aprentissage.
                    Je vous invite à revenir de temps en temps sur mon portfolio pour voir les nouveautés qui peuvent arriver dessus.

                    <br><br><br><a href="https://clement-menager.tk/" target="_blank" class="btn port">Voir le projet</a>
                </p>
            </div>
        </div>


    </div>
</div>
<div class="cursor"></div>

@endsection
