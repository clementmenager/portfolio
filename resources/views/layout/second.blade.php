<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Clément-ménager</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="{{ asset('images/Me.png') }}" type="image/png">

    <!-- LARAVEL -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{-- CSS --}}
    <link rel="stylesheet" href="{{ mix('css/projet.css') }}">
    <link rel="stylesheet" href="{{ mix('css/transition.css') }}">




    {{-- JS --}}
    {{-- <script src="{{ mix('js/fullpage.js') }}" defer></script> --}}
    <script src="{{ mix('js/projet.js') }}" defer></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/3.1.1/fullpage.min.js" integrity="sha512-7r/OesWXSCkeaAN9IA8BCzedivXu9M0eLGt+g+jzrvNHakOlxxqc9fknv4uRRvzfCQLmUh0LX2JpT2NcbyrOUA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>


</head>
<body>
    <div class="transion transition-1 is-active">
        <div class="wrapper">
            <div class="typing-demo">
              Loading...
            </div>
        </div>
    </div>

    <nav>
        <a href="{{ route('accueil')."#mes-projets" }}">Retour</a>

        <li class="slide"></li>
    </nav>
    @yield('content')


</body>
</html>
