<?php 
    session_start();

    class Connexion
    {
        private $_user = "";
        private $_psw = "";

        public function __construct($ut, $mdp)
        {
            $this->_user = $ut;
            $this->_psw = $mdp;
        }

        public function get_user()
        {
            return $this->_user;
        }
        
        public function get_psw()
        {
            return $this->_psw;
        }
        
        public function set_user($a)
        {
            $this->_user = $a;
        }
        
        public function set_psw($b)
        {
            $this->_psw = $b;
        }

        public function verification($con, $utilisateur, $motdepasse)
        {
            $s = $con->query("SELECT * FROM login");
            foreach($s as $ligne)
            {
                if($ligne['user']==$utilisateur && $ligne['psw']==$motdepasse)
                {
                    header("location:accueil.php");
                    $_SESSION['ut'] = $utilisateur;
                    $_SESSION['psw'] = $motdepasse;
                }
                else
                {
                    ?>
                    <script>
                        alert("L'utilisateur ou le mot de passe est incorrect, veuillez réessayer");
                    </script>
                    <?php
                }
            }
        }

        public function verifPage($con1, $u, $m)
        {
            $sql = $con1->query("SELECT * FROM login");
            foreach($sql as $ligne)
            {
                if($ligne['user']!==$u || $ligne['psw']!==$m)
                {
                    header("location:index.php");
                    return "Vous ne vous êtes pas identifier !";
                }
                else 
                {

                }
            }
        }
    }
?>